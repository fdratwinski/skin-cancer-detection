import numpy as np
import pandas as pd
import os
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras_preprocessing.image import ImageDataGenerator


class DataPrep:
    def __init__(self, csv_path, percentage_train_dataset, percentage_val_dataset, percentage_test_dataset, image_size):
        self.image_size = image_size
        self.file_path = csv_path
        self.percentage_train_dataset = percentage_train_dataset
        self.percentage_val_dataset = percentage_val_dataset
        self.percentage_test_dataset = percentage_test_dataset
        self.x_train, self.y_train, \
        self.x_val, self.y_val, \
        self.x_test, self.y_test = self.split_and_prepare_datasets()
        self.training_generator, self.validation_generator, self.test_genereator, self.test_inedexes = self.prepare_data()

    def split_and_prepare_datasets(self):
        if self.percentage_test_dataset + self.percentage_val_dataset + self.percentage_train_dataset != 1.0:
            print('ERROR, Percentages dont sum up to 100%')
        data = pd.read_csv(self.file_path)
        data['image_full_name'] = data['image_id'] + '.jpg'
        X = data[['image_full_name', 'dx', 'lesion_id']]

        Y = X.pop('dx').to_frame()
        x_train, x_remain, y_train, y_remain = train_test_split(X, Y, test_size=(
                self.percentage_val_dataset + self.percentage_test_dataset), random_state=42)
        new_test_size = np.around(
            self.percentage_test_dataset / (self.percentage_val_dataset + self.percentage_test_dataset), 2)
        x_val, x_test, y_val, y_test = train_test_split(x_remain, y_remain, test_size=new_test_size, random_state=42)

        return x_train, y_train, x_val, y_val, x_test, y_test

    def prepare_data(self):
        train = pd.concat([self.x_train, self.y_train], axis=1)
        val = pd.concat([self.x_val, self.y_val], axis=1)
        test = pd.concat([self.x_test, self.y_test], axis=1)

        name_as_indexes_testing = self.encode_labels(train, val, test)

        training_datagen, validation_datagen, test_datagen = self.get_data_generators()
        train_gen, val_gen, test_gen = self.link_generators_with_dataframe(
            train_dataframe=train,
            train_datagen=training_datagen,
            val_dataframe=val,
            val_datagen=validation_datagen,
            test_dataframe=test,
            test_datagene=test_datagen)
        return train_gen, val_gen, test_gen, name_as_indexes_testing

    def encode_labels(self, train_df, val_df, test_df):
        encoder = LabelEncoder()
        encoder.fit(train_df['dx'])
        name_as_indexes_train = encoder.transform(train_df['dx'])
        train_df['label'] = name_as_indexes_train

        encoder = LabelEncoder()
        encoder.fit(val_df['dx'])
        name_as_indexes_val = encoder.transform(val_df['dx'])
        val_df['label'] = name_as_indexes_val

        encoder = LabelEncoder()
        encoder.fit(test_df['dx'])
        name_as_indexes_test = encoder.transform(test_df['dx'])
        test_df['label'] = name_as_indexes_test

        return name_as_indexes_test

    def get_data_generators(self):
        train_datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=120,
            zoom_range=0.10,
            horizontal_flip=True,
            width_shift_range=0.10,
            height_shift_range=0.10)

        val_datagen = ImageDataGenerator(rescale=1. / 255.)
        test_datagene = ImageDataGenerator(rescale=1. / 255)
        return train_datagen, val_datagen, test_datagene

    def link_generators_with_dataframe(self,
                                       train_dataframe, val_dataframe, test_dataframe,
                                       train_datagen, val_datagen, test_datagene):
        train_generator = train_datagen.flow_from_dataframe(
            dataframe=train_dataframe,
            x_col="image_full_name",
            y_col="dx",
            batch_size=64,
            directory="resources/images",
            shuffle=True,
            class_mode="categorical",
            target_size=self.image_size
        )

        validation_generator = val_datagen.flow_from_dataframe(
            dataframe=val_dataframe,
            x_col="image_full_name",
            y_col="dx",
            batch_size=64,
            directory="resources/images",
            shuffle=True,
            class_mode="categorical",
            target_size=self.image_size
        )

        test_generator = test_datagene.flow_from_dataframe(
            dataframe=test_dataframe,
            x_col="image_full_name",
            y_col="dx",
            directory="resources/images",
            shuffle=False,
            batch_size=1,
            class_mode=None,
            target_size=self.image_size)

        return train_generator, validation_generator, test_generator

    def generate_images(label_name, value):
        data = pd.read_csv('resources/HAM10000_metadata.csv')
        data['image_full_name'] = data['image_id'] + '.jpg'
        X = data[['image_full_name', 'dx', 'lesion_id']]
        label_X = X[X['dx'] == label_name]
        label_Y = label_X.pop('dx').to_frame()
        concated = pd.concat([label_X, label_Y], axis=1)

        train_datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=20,
            zoom_range=0.10,
            brightness_range=(0.65, 1.35),
            width_shift_range=0.05,
            height_shift_range=0.05)

        j = 0
        for i in train_datagen.flow_from_dataframe(
                dataframe=concated,
                x_col="image_full_name",
                y_col="dx",
                batch_size=64,
                directory="resources/images",
                shuffle=False,
                class_mode="categorical",
                target_size=(600, 600),
                save_to_dir="resources/generated/" + label_name,
                save_prefix='generated'
        ):
            j += 1
            if j > value:
                break

        list_of_generated_files = os.listdir('resources/generated/' + label_name)
        list_of_generated_files = np.asarray(list_of_generated_files)
        generated_labels = [np.asarray(["", label_name]) for x in range(len(list_of_generated_files))]
        generated_labels = np.asarray(generated_labels)
        generated_dataframe = pd.DataFrame(np.column_stack((list_of_generated_files, generated_labels)),
                                           columns=['image_full_name', 'lesion_id', 'dx'])
        return generated_dataframe
