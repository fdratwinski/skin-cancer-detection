import matplotlib.pyplot as plt
import numpy as np
import pickle as pkl
import pandas as pd
import tensorflow as tf
from keras.applications import InceptionV3
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from sklearn.metrics import confusion_matrix
from keras import Model
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras import optimizers

from com.skincheck.DataPrep import DataPrep


class CNNModel:
    def __init__(self, image_size, name_of_last_layer_inception, learning_rate):
        self.learning_rate = learning_rate
        self.image_size = image_size
        inception_model = self.get_inception_model(name_of_last_layer_inception, image_size)
        self.model = self.make_model(inception_model)

    def get_inception_model(self, name_of_last_layer, img_size):
        pre_trained_model = InceptionV3(input_shape=(img_size[0], img_size[1], 3),
                                        include_top=False,
                                        weights="imagenet")
        pre_trained_model_input = pre_trained_model.get_layer(index=0).input
        pre_trained_model_output = pre_trained_model.get_layer(name_of_last_layer).output
        my_inception = Model(inputs=pre_trained_model_input,
                             outputs=pre_trained_model_output)
        return my_inception

    def make_model(self, inception_network):
        mdel = Sequential()
        mdel.add(inception_network)
        mdel.add(Conv2D(64, (3, 3), activation='relu'))
        mdel.add(MaxPooling2D(pool_size=(2, 2)))
        mdel.add(Conv2D(64, (2, 2), activation='relu'))
        mdel.add(MaxPooling2D(pool_size=(2, 2)))
        mdel.add(Flatten())
        mdel.add(Dense(512, activation='relu'))
        mdel.add(Dense(256, activation='relu'))
        mdel.add(Dense(128, activation='relu'))
        mdel.add(Dropout(0.5))
        mdel.add(Dense(7, activation='softmax'))

        mdel.compile(optimizer=optimizers.adam(lr=self.learning_rate),
                     loss='categorical_crossentropy',
                     metrics=['acc'])
        mdel.summary()
        return mdel

    def check_model_on_test_set(self, test_dataset, name_as_indexes_test):
        self.model.load_weights('resources/weights.hdf5')
        test_dataset.reset()
        predictions = self.model.predict_generator(
            test_dataset,
            steps=test_dataset.samples / test_dataset.batch_size,
            verbose=1)
        y_pred = np.argmax(predictions, axis=1)
        cm = confusion_matrix(name_as_indexes_test, y_pred)
        print(cm)
        print('Full percentage:')
        number_of_predicted_good = cm[0][0] + cm[1][1] + cm[2][2] + cm[3][3] + cm[4][4] + cm[5][5] + cm[6][6]
        print(number_of_predicted_good / test_dataset.samples * 100)
        number_of_predicted_good = number_of_predicted_good - cm[5][5]
        number_of_samples_without_label_5 = test_dataset.samples - np.sum(cm[5])
        print('Percentage without label 5:')
        print(number_of_predicted_good / number_of_samples_without_label_5 * 100)
        for i in range(cm.shape[0]):
            print("{} {}".format("Percentage for label:", i))
            percentage = cm[i][i] / np.sum(cm[i])
            print(percentage)

    def print_and_save_charts(self, history):
        acc = history.history['acc']
        val_acc = history.history['val_acc']
        loss = history.history['loss']
        val_loss = history.history['val_loss']

        plt.plot(acc, 'r', label='Training accuracy')
        plt.plot(val_acc, 'b', label='Validation accuracy')
        plt.title('Training and validation accuracy')
        plt.legend(loc=0)
        plt.figure()

        plt.show()
        plt.savefig('resources/acc.png')

        plt.plot(loss, 'r', label='Training loss')
        plt.plot(val_loss, 'b', label='Validation loss')
        plt.title('Training and validation loss')
        plt.legend(loc=0)
        plt.figure()

        plt.show()
        plt.savefig('resources/loss.png')

    def run_training(self, data_prep, epochs, class_weights=None):
        checkpointer = ModelCheckpoint('resources/weights.hdf5', monitor='val_loss', verbose=1, save_best_only=True)
        learning_control = ReduceLROnPlateau(monitor='val_loss', patience=3, verbose=1, factor=.5, min_lr=0.00001)
        callback = validation_acc_callback()
        model_history = self.model.fit_generator(generator=data_prep.training_generator,
                                                 steps_per_epoch=data_prep.training_generator.samples // data_prep.training_generator.batch_size,
                                                 validation_data=data_prep.validation_generator,
                                                 validation_steps=data_prep.validation_generator.samples // data_prep.validation_generator.batch_size,
                                                 class_weight=class_weights,
                                                 verbose=1,
                                                 use_multiprocessing=False,
                                                 epochs=epochs, callbacks=[learning_control, checkpointer, callback])
        self.print_and_save_charts(model_history)
        self.check_model_on_test_set(data_prep.test_genereator, data_prep.test_inedexes)

    def base64_string_to_numpy_array(self, string):
        jpg = tf.image.decode_jpeg(string, channels=3)
        jpg = tf.image.resize_images(jpg, size=self.image_size, method=tf.image.ResizeMethod.BILINEAR,
                                     align_corners=False)
        return jpg

    def predict_disease(self, img_str):
        self.model.load_weights('resources/weights.hdf5')
        result = self.model.predict(img_str)
        return result


class validation_acc_callback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if logs.get('val_acc') > 0.90:
            print("\nReached 90% validation accuracy so cancelling training!")
            self.model.stop_training = True


if __name__ == '__main__':
    image_size = (299, 299)
    epochs = 20
    learning_rate = 0.0001

    class_weight = {0: 100.0,
                    1: 25.0,
                    2: 20.0,
                    3: 100.0,
                    4: 20.0,
                    5: 1.5,
                    6: 30.0}
    model = CNNModel(image_size, 'mixed10', learning_rate)
    data_preparator = DataPrep('resources/HAM10000_metadata.csv', 0.7, 0.2, 0.1, image_size)
    model.run_training(data_preparator, epochs)
