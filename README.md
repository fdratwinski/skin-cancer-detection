# Convolutional neural network

This application is used to research and test options and parameters of the convolutional neural network model.

For the software to work, the user needs to extract two zip files provided on https://www.kaggle.com/kmader/skin-cancer-mnist-ham10000 to this directory:
`src/com/skincheck/resources/images`. That way, a learning can be started. 